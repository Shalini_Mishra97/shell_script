#!/bin/bash

file=$1

if [ $# -eq 0 ]
then
	read -p "Please provide input" file

elif [ $# -gt 1 ]
then
	
	echo "Only one argument expected,if filename has spaces in it use double quotes in argument"
	exit 0

fi


a=$PWD
echo $a

if [ -f $a/$file ]
	
then
	echo "File already exist"

else
	date > $a/$file

fi


