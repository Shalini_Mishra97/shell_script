#!/bin/bash

num=$1

if [ $# -eq 0 ]
then
	read -p "Please provide the number: " num

fi

i=1

while [ $i -le 10 ]
do
	mult=`expr $num \* $i`
	echo $mult
	i=`expr $i + 1`
done
